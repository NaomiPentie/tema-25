<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use App\Entity\Menu;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
class MenuController extends AbstractController
{
    /**
     * @Route("/menu", name="menu")
     */
    public function new(Request $request)
    {
        $menu = new Menu();
 
        $form = $this->createFormBuilder($menu)
            ->add('Name', TextType::class)
            ->add('Tip', TextType::class)
            ->add('Price', IntegerType::class)
            ->add('save', SubmitType::class, ['label' => 'Create Menu'])
            ->getForm();
        $form->handleRequest($request);
 
        if ($form->isSubmitted() && $form->isValid()) {
            $menu = $form->getData();
 
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($menu);
            $entityManager->flush();
 
            return new Response('Menu created!');
        }
 
        return $this->render('menu/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
